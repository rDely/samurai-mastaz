﻿using UnityEngine;
using System.Collections;

public class Missile : MonoBehaviour {

	bool active = false;
	//Feedbacks for the explosion : The sprite,
	public Sprite explosionSprite;
	//and the sound
	public AudioClip explosionSound;
	//and the beeping sound it makes when it detects a player
	public AudioClip bipSound;
	public GameObject explosion;

	//Size of the explosion (in Awake this will take the size of the sprite)
	float explosionRadius;
	//How long does the missile waits between two rotations towards target
	float timeWait;
	//How fast does the missile goes
	float missileSpeed = 25f;
	//Logic tool to prevent the missile from moving once it exploded
	bool exploding; 

	void Awake () {
		explosionRadius = explosionSprite.bounds.extents.x;
	}

	void Start()
	{
		StartCoroutine ("WaitForAction");
	}
	
	//If you touch something, it goes boom
	void OnCollisionEnter2D (Collision2D coll) {
		if(active)
		  StartCoroutine ("Boom");
	}

	IEnumerator WaitForAction()
	{
		yield return new WaitForSeconds (.2f);
		active = true;
	}

	//If a player is in the detection trigger zone, the missile will pursue him
	void OnTriggerStay2D(Collider2D coll){
		if (coll.tag == "Player" && Time.time > timeWait)
			Seek(coll.gameObject);
	}

	//Speed is constantly updated so that it always goes towards what it's facing
	//Unless the Rigidbody is asleep
	void FixedUpdate(){
		if(!GetComponent<Rigidbody2D>().IsSleeping())
		GetComponent<Rigidbody2D> ().velocity = transform.right * missileSpeed;
	}

	//Adjust trajectory to find target
	void Seek(GameObject target)
	{
		timeWait = Time.time + .5f;
		AudioSource.PlayClipAtPoint (bipSound, Vector2.zero);
		transform.right = (target.transform.position - transform.position).normalized;
	}



	IEnumerator Boom()
	{
		//Make the rigidbody sleep.
		GetComponent<Rigidbody2D>().Sleep();
		//Reposition the sprite before switching to explosion so the feeback isn't crooked,
		transform.rotation = new Quaternion(0,0,0,0);
		transform.right = Vector2.right;

		//change the sprite,
		Instantiate(explosion, transform.position, Quaternion.identity);


		//DESTROY EVERYTHING IN RANGE
		foreach (Collider2D coll in Physics2D.OverlapCircleAll(transform.position, explosionRadius)) {
			//PEOPLES !
			if(coll.tag == "Player")
				Data.data.Die(coll.gameObject);
			//WOODEN CRATES !
			else if(coll.tag == "Crate" && coll.GetComponent<CrateScript>().crateType == CrateScript.CrateType.wooden)
				Destroy (coll.gameObject);
			//POWER UPS !
			else if (coll.tag == "Collectible")
				Destroy(coll.gameObject);
		}
		//Play a nice BOOM sound
		AudioSource.PlayClipAtPoint (explosionSound, transform.position);
		//Wait to give the player the player a little feedback on the situation
		yield return new WaitForSeconds (.3f);
		//Bye bye missile
		Destroy (gameObject);
	}
}
