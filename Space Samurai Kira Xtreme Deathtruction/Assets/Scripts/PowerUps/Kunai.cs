using UnityEngine;
using System.Collections;

public class Kunai : MonoBehaviour {

	bool active = false;
	// Use this for initialization
	void Start () {
		StartCoroutine ("Lifetime");
	}
	
	//Destroy stuff when you collide with them
	void OnCollisionEnter2D (Collision2D coll) {
		if (coll.collider.tag == "Player" && active)
			Data.data.Die (coll.collider.gameObject);
		else if (coll.collider.tag == "Back" && active)
			Data.data.Die (coll.collider.transform.parent.gameObject);
		else if (coll.collider.tag == "Crate" && coll.collider.GetComponent<CrateScript>().crateType == CrateScript.CrateType.wooden)
			Destroy (coll.collider.gameObject);
	}

	//The time before it disappears
	IEnumerator Lifetime()
	{
		yield return new WaitForSeconds (.2f);
		active = true;
		yield return new WaitForSeconds(3f);
		Destroy (this.gameObject);
	}
}
