﻿using UnityEngine;
using System.Collections;

public class Dynamite : MonoBehaviour {


	//Countdown sound
	public AudioClip bipSound;
	//Explosion sound
	public AudioClip boomSound;
	//Explosion sprite
	public Sprite explosionSprite;
	public GameObject explosion;

	//Size of the explosion (in Awake this will take the size of the sprite)
	float explosionRadius;


	void Awake()
	{
		//Get the size of the sprite
		explosionRadius = explosionSprite.bounds.extents.x;
	}

	void Start () {
		//Initiate the (FINAL) countdown (TUTUTUDU TUTUDUTUTU)
		StartCoroutine ("Countdown");
	}

	//BIP, BIP, BIP, BIP
	IEnumerator Countdown()
	{
		AudioSource.PlayClipAtPoint (bipSound, transform.position);
		yield return new WaitForSeconds (.5f);
		AudioSource.PlayClipAtPoint (bipSound, transform.position);
		yield return new WaitForSeconds (.5f);
		AudioSource.PlayClipAtPoint (bipSound, transform.position);
		yield return new WaitForSeconds (.5f);
		AudioSource.PlayClipAtPoint (bipSound, transform.position);
		yield return new WaitForSeconds (.5f);
		StartCoroutine ("Boom");
	}

	//BOOM
	IEnumerator Boom()
	{
		//Make the rigidbody sleep
		GetComponent<Rigidbody2D> ().Sleep ();
		//Reposition the sprite before switching to explosion so the feeback isn't crooked,
		transform.rotation = new Quaternion(0,0,0,0);
		transform.right = Vector2.right;
		//Play the sound
		AudioSource.PlayClipAtPoint (boomSound, transform.position);
		//Show the sprite
		Instantiate(explosion, transform.position, Quaternion.identity);

		//DESTROY EVERYTHING IN RANGE
		foreach (Collider2D coll in Physics2D.OverlapCircleAll(transform.position, explosionRadius)) {
		if(coll.tag == "Player")
				Data.data.Die(coll.gameObject);
		else if(coll.tag == "Crate" && coll.GetComponent<CrateScript>().crateType == CrateScript.CrateType.wooden)
				Destroy (coll.gameObject);
			else if (coll.tag == "Collectible")
				Destroy(coll.gameObject);
		}
		//Give a little time to mourn
		yield return new WaitForSeconds (.3f);
		//And bye
		Destroy (gameObject);
	}
}
