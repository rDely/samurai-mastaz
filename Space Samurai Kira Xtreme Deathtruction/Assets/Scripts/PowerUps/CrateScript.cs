﻿using UnityEngine;
using System.Collections;

public class CrateScript : MonoBehaviour {

	//Hello I'm a crate, we are different kinds
	public enum CrateType { spiked, wooden, steel};
	public CrateType crateType;
	public AudioClip extraBulletSound;

	void Start()
	{
		Data.data.powerUp = gameObject;
	}

	//Sometimes people hit me
	public void Hit (Vector2 force, PlayerScript player) {
		//And my woody sisters die
		if (crateType == CrateType.wooden)
			{
				//And loot bullets
				int lootRandomizer = Random.Range (0, 100);
					if (lootRandomizer > 65)
						{
						AudioSource.PlayClipAtPoint (extraBulletSound, this.transform.position);
						player.ExtraBullet();
						}
				//And die =(
				Destroy(gameObject);
			}
		//But my other sisters just get pushed
		else 
			this.GetComponent<Rigidbody2D> ().velocity = force;
	}

	//Otherwise, when my spiky brothers hit something
	void OnCollisionEnter2D(Collision2D coll)
	{
		//They KILL IT, FOR REVENGE
		if (crateType == CrateType.spiked && coll.collider.tag == "Player")
			Data.data.Die(coll.collider.gameObject);
	}
}
