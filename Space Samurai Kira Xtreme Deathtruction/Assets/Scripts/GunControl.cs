﻿using UnityEngine;
using System.Collections;

public class GunControl : MonoBehaviour {


	//What player is that gun linked to
	public int playerNumber;
	bool slashing;
	bool facingRight = true;
	float invertAxis = 1f;
	//Get the player number
	void Awake()
	{
		playerNumber = transform.parent.GetComponent<PlayerScript> ().playerNumber;
	}

	public void Slashing()
	{
		slashing = !slashing;
	}


	//Gamepad based gun orientation
	void FixedUpdate () {	
		float horizontalFlip = Input.GetAxis ("Horizontal" + playerNumber.ToString ());
		float verticalFlip = invertAxis * Input.GetAxis ("Vertical"+playerNumber.ToString());
		/*if (facingRight && horizontalFlip < 0)
			Flip ();
		else if (!facingRight && horizontalFlip > 0)
			Flip ();
	*/
	if(!slashing)
			transform.right = Vector3.Normalize (new Vector3 (horizontalFlip, Input.GetAxis ("Vertical"+playerNumber.ToString()), 0));
		
	}
	/*
	void Flip()
	{
		facingRight = !facingRight;
		invertAxis *= -1f;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
	*/
}
