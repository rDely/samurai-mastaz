﻿using UnityEngine;
using System.Collections;

public class Bumpers : MonoBehaviour {

	// Update is called once per frame
	void OnCollisionStay2D(Collision2D coll)
	{
		if (coll.collider.GetComponent<Rigidbody2D> () != null)
			coll.collider.GetComponent<Rigidbody2D> ().velocity = - transform.position;
		else if (coll.collider.tag == "Back")
			coll.collider.transform.parent.GetComponent<Rigidbody2D> ().velocity = - transform.position;
	}
	/*
	void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.collider.GetComponent<Rigidbody2D> () != null)
			coll.collider.GetComponent<Rigidbody2D> ().velocity = - transform.position;
		else if (coll.collider.tag == "Back")
			coll.collider.transform.parent.GetComponent<Rigidbody2D> ().velocity = - transform.position;
	}
*/
}
