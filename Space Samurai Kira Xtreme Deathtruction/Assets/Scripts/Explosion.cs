﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {

	
	public float minRand;
	public float maxRand;
	public AnimationClip explosionAnim;

	void Start()
	{
		StartCoroutine ("Shake", explosionAnim.length);
	}

	IEnumerator Shake(float f)
	{

		for(int i = 0; i <10 ; i++)
		{
			float randomX = Random.Range (minRand, maxRand);
			float randomY = Random.Range (minRand, maxRand);
			Camera.main.transform.position = new Vector3 (randomX, randomY, Camera.main.transform.position.z);
			yield return new WaitForSeconds (f/20);
			Camera.main.transform.position = new Vector3 (0, 0, Camera.main.transform.position.z);
			yield return new WaitForSeconds (f/20);
		}
		Destroy (gameObject);
	}

}
