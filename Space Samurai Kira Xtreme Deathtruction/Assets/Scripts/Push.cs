﻿using UnityEngine;
using System.Collections;

public class Push : MonoBehaviour {

	public float pushforce = 40f;
	// Use this for initialization
	void Awake () {
		GetComponent<PolygonCollider2D> ().enabled = false;
		GetComponent<SpriteRenderer> ().color = Color.clear;
	}
	
	// Update is called once per frame
	void OnCollisionStay2D(Collision2D coll)
	{
		if (coll.collider.GetComponent<Rigidbody2D> () != null)
			coll.collider.GetComponent<Rigidbody2D> ().velocity = transform.parent.parent.GetComponent<PlayerScript> ().gunEnd.right * pushforce;;
	}
}
