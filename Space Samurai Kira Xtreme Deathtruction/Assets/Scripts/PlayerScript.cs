using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour {



	//OK LET'S FUCKING CLEAN THAT UP

	//VARIABLES

	//PREFABS
	public GameObject bullet;
	public GameObject grenade;
	public GameObject missile;
	public GameObject kunai;
	public GameObject arrow;
	public GameObject shell;

	//Other part of the playerObject
	public GameObject back;
	public Transform sword;
	public Transform gunEnd;
	public GameObject gunSprite;
	public GameObject playerSprite;
	public PolygonCollider2D pushZone;
	public Text nametag;

	//LOGIC TOOLS -------
	//Are you slashing right now ?
	public bool slashing;
	//
	bool invulnerable;
	float grappleCooldown;
	float hitCooldown;
	public LayerMask walls;
	//SOUNDS
	public AudioClip pary;

	Collectible.ConsumableType currentPwrUp = Collectible.ConsumableType.none;

	public LayerMask shootMask;

	//I - PLAYER DATA

	//For flipping the sprite

	//Health & associated GUI content
	public float health;

	//Multiplayer data
	public int playerNumber;
	int pistolBullet = 3;

	//Gun stuff
	Weapon currentWeapon;

	//Tweaking stuff
	float maxGrappleSize = 80f;
	float pullForce = 60f;
	float pushForce = 26f;
	float shellAngularSpeed = 100f;
	float grenadeThrust = 40f;

	//The size of the grapple body
	float currentGrappleSize;
	Vector3 grappleAttachPosition;
	List<string> otherPlayersMask = new List<string> ();
	//Prefabs for the grapple
	public GameObject grappleBody;
	
	//Instantiated part of the grappling hook
	GameObject bodyInstance;
	//Trying stuff
	bool triggerReleased = true;

	void Awake()
	{
		otherPlayersMask.Add("Grappling");
		otherPlayersMask.Add("P1");
		otherPlayersMask.Add("P2");
		otherPlayersMask.Add("P3");
		otherPlayersMask.Add("P4");
		//Assign the distance joint (that'll will be the physic reality of the grappling hook)
		health = 100;
		currentWeapon = new Weapon(Weapon.WeaponType.gun);
		gunSprite.GetComponent<SpriteRenderer> ().sprite = currentWeapon.weaponSprite;
	}

	void Start()
	{
		
		otherPlayersMask.RemoveAt (playerNumber);
		foreach (string s in otherPlayersMask) {
			print (s);
		}
		shootMask = LayerMask.GetMask (otherPlayersMask[0], otherPlayersMask[1], otherPlayersMask[2], otherPlayersMask[3]);
		switch (playerNumber) {
		case 1 :
			playerSprite.GetComponent<SpriteRenderer>().color = Color.white;
			break;
		case 2 :
			playerSprite.GetComponent<SpriteRenderer>().color = Color.yellow;
			break;
		case 3 :
			playerSprite.GetComponent<SpriteRenderer>().color = Color.red;
			break;
		default :
			playerSprite.GetComponent<SpriteRenderer>().color = Color.green;
			break;
		}
	}
	
	//III - GENERATE THE GRAPPLE
	void Update() 
	{
		if (Data.data.gameOn) {
			slashing = sword.GetComponent<SwordScript> ().parying;
			if (Input.GetButtonDown ("Melee" + playerNumber) && Time.time >= hitCooldown) {
				hitCooldown = Time.time + .5f;
				sword.gameObject.SetActive (true);
				sword.SendMessage ("Slash");
			}
			if (Input.GetButtonDown ("UsePwrUp" + playerNumber)) {
				UsePowerUp ();
			}
			//Shooting the grapple
			//if (Input.GetAxis ("Grapple" + playerNumber.ToString ()) != 0 && Time.time > grapplerCooldown && triggerReleased && WallCheck()) 
			if (Input.GetAxis ("Grapple" + playerNumber.ToString ()) != 0 && Time.time > grappleCooldown && triggerReleased) {
				ShootGrapple (Mathf.Sign (Input.GetAxis ("Grapple" + playerNumber.ToString ())));
				grappleCooldown = Time.time + .25f;
				triggerReleased = false;
			}
			if (!triggerReleased && Input.GetAxis ("Grapple" + playerNumber.ToString ()) == 0) {
				triggerReleased = true;
			}
			//SHOOTING
			if (Input.GetButtonDown ("Shoot" + playerNumber.ToString ()) && currentWeapon.ammo > 0 && WallCheck ())
				StartCoroutine ("Shoot");
		}
	}

	//Reset is for cases when you had another weapon and you switch back to gun when you're out of ammo
	public void AssignWeapon (Weapon.WeaponType wpn)
	{
		if (currentWeapon.weaponType == Weapon.WeaponType.gun)
			pistolBullet = currentWeapon.ammo;
 		currentWeapon = new Weapon (wpn);
		gunSprite.GetComponent<SpriteRenderer> ().sprite = currentWeapon.weaponSprite;

	}

	//Override so you can switch back to gun when you're out of ammo but don't get the initial bullets
	public void AssignWeapon ()
	{
		currentWeapon = new Weapon (Weapon.WeaponType.gun);
		currentWeapon.ammo = pistolBullet;
		gunSprite.GetComponent<SpriteRenderer> ().sprite = currentWeapon.weaponSprite;
	}

	void ShootGrapple(float f)
	{
		float grappleRange = maxGrappleSize;
		//A - INSTANTIATE OBJECTS

		//PULL
		if (f > 0) 
		{
			//Instantiate the grapple body
			bodyInstance = Instantiate(grappleBody) as GameObject;
			//Set parent & Adjust position, rotation and scale
			bodyInstance.transform.parent = transform;
			bodyInstance.transform.position = transform.position;
			bodyInstance.transform.right = gunEnd.right;
			bodyInstance.GetComponent<SpriteRenderer> ().color = Color.red;
			// Raycast from the barrel end and at the direction corresponding to gamepad input
			RaycastHit2D hit = Physics2D.Raycast(gunEnd.position, gunEnd.right, grappleRange, shootMask);
			//Did you hit something ?
			if (hit.collider != null) {
				grappleAttachPosition = hit.point;
				//Logic & sprite
				currentGrappleSize = Vector2.Distance (hit.point, transform.position);
				
				if (hit.collider.tag != "Hazard") 
				{

					if (hit.collider.tag == "Collectible")
						StartCoroutine ("Collect", hit.collider.gameObject);
					else if (hit.collider.tag == "Structure")
						StartCoroutine ("PullYourself");
					else
						StartCoroutine ("Pull", hit.collider.gameObject);
				}
				else
					StartCoroutine("grappleCollapse");
			}
			//Or launch coroutine to collapse the grapple
			else 
			{
				currentGrappleSize = grappleRange;
				StartCoroutine("grappleCollapse");
			}
			//Place the middle sprite
			bodyInstance.transform.localScale = new Vector3(currentGrappleSize, 1f, 1f);
			bodyInstance.transform.position = transform.position;
			bodyInstance.transform.localScale = new Vector3(currentGrappleSize, 1f, 1f);
			bodyInstance.transform.position = transform.position;

		}
		//PUSH
		else
		{
			//Rewrite the whole script there
			StartCoroutine("Push");
		}

	}

	IEnumerator Collect(GameObject gO)
	{
		if (gO.GetComponent<Collectible> ().type == Collectible.PwrUpType.weapon)
			AssignWeapon (gO.GetComponent<Collectible> ().wpnTp);
		else if (gO.GetComponent<Collectible> ().type == Collectible.PwrUpType.consumable)
			AssignPowerUp (gO.GetComponent<Collectible> ().pwrUp);
		        Destroy (gO);
		yield return new WaitForSeconds (.1f);
		Collapse ();
	}
	
	IEnumerator PullYourself()
	{
		yield return new WaitForSeconds (.1f);
		Collapse ();
		GetComponent<Rigidbody2D> ().velocity = pullForce * (grappleAttachPosition - transform.position).normalized;
		
	}
	
	IEnumerator Pull(GameObject gO)
	{
		yield return new WaitForSeconds (.1f);
		Collapse ();
		if (gO != null) {
			if (gO.tag == "Back")
				gO.transform.parent.GetComponent<Rigidbody2D> ().velocity = -pullForce * (grappleAttachPosition - transform.position).normalized;
			else
				gO.GetComponent<Rigidbody2D> ().velocity = -pullForce * (grappleAttachPosition - transform.position).normalized;
		}
	}

	IEnumerator Push(GameObject gO)
	{
		pushZone.enabled = true;
		pushZone.gameObject.GetComponent<SpriteRenderer> ().color = Color.blue;
		yield return new WaitForSeconds (.3f);
		pushZone.enabled = false;
		pushZone.gameObject.GetComponent<SpriteRenderer> ().color = Color.clear;

	}

	IEnumerator grappleCollapse()
	{
		yield return new WaitForSeconds(.1f);
		Destroy (bodyInstance);
	}
	void Collapse()
	{
		Destroy (bodyInstance);
	}


	public IEnumerator LoseHealth (float f)
	{
		if (!invulnerable) {
			health -= f;
			if (health <= 0)
				Data.data.Die (gameObject);
			yield return new WaitForSeconds(.12f);
			invulnerable = true;
			yield return new WaitForSeconds(.12f);
			invulnerable = false;
		}
	}

	void UsePowerUp()
	{
		switch (currentPwrUp) {
		case Collectible.ConsumableType.grenade :
			GameObject gre = Instantiate(grenade) as GameObject;
			gre.transform.position = gunEnd.transform.position;
			gre.transform.rotation = gunEnd.rotation;
			gre.GetComponent<Rigidbody2D>().velocity = gunEnd.right.normalized * grenadeThrust;
			gre.GetComponent<Rigidbody2D>().angularVelocity = 2000f;
			break;
		case Collectible.ConsumableType.kunai : 
			GameObject kun = Instantiate(kunai) as GameObject;
			kun.transform.position = gunEnd.transform.position;
			kun.transform.rotation = gunEnd.rotation;
			kun.GetComponent<Rigidbody2D>().velocity = gunEnd.right * 50f;
			kun.GetComponent<Rigidbody2D>().angularVelocity = 2000f;
			break;
		case Collectible.ConsumableType.missile :
			GameObject mis = Instantiate(missile) as GameObject;
			mis.transform.position = gunEnd.transform.position;
			mis.transform.rotation = gunEnd.rotation;
			mis.GetComponent<Rigidbody2D>().velocity = gunEnd.right * pushForce;
			break;
		
		}
		currentPwrUp = Collectible.ConsumableType.none;
	}



	public void AssignPowerUp (Collectible.ConsumableType pwr)
	{
		currentPwrUp = pwr;
	}

	public void ExtraBullet()
	{
		currentWeapon.ammo ++;
	}

	//SHOOTING YOUR WEAPON
	IEnumerator Shoot()
	{


		switch (currentWeapon.weaponType) 
			{
			case(Weapon.WeaponType.bow) :
				ShootStuff ();
				//Set parent & Adjust position, rotation and scale of the feedback
				bullet.transform.position = gunEnd.position;
				bullet.transform.rotation = gunEnd.rotation;
				ShootBow(gunEnd.transform.right);
				AssignWeapon();
				break;
			case(Weapon.WeaponType.gun) :
				ShootStuff ();
				//Set parent & Adjust position, rotation and scale of the feedback
				bullet.transform.position = gunEnd.position;
				bullet.transform.rotation = gunEnd.rotation;
				ShootBullet(gunEnd.transform.right);
					break;
			case(Weapon.WeaponType.machineGun) :
				for (int i = 0; i < currentWeapon.ammo; i++) 
					{
						ShootStuff ();
						float bulletSpread = Random.Range(-.2f, .2f);
						Vector3 bulletDirection = gunEnd.transform.right + new Vector3(bulletSpread, bulletSpread, 0);
						//Add force of recoil
						GetComponent<Rigidbody2D> ().AddForce(-(gunEnd.position - transform.position).normalized * currentWeapon.power);
						ShootBullet(bulletDirection);
						yield return new WaitForSeconds (.05f);
					}
					AssignWeapon ();
					break;
			case(Weapon.WeaponType.shotgun) :
				float[] bulletSpreadArray = {
					Random.Range (-.5f, .5f),
					Random.Range (-.5f, .5f),
					Random.Range (-.5f, .5f),
					Random.Range (-.5f, .5f),
					Random.Range (-.5f, .5f),
					Random.Range (-.5f, .5f),
			};
				
				foreach (float spread in bulletSpreadArray)
				{
					Vector3 bulletDirection = gunEnd.transform.right + new Vector3(spread, spread, 0);
					ShootBullet(bulletDirection);
				}
				ShootStuff ();
				currentWeapon.ammo = 0;
				AssignWeapon ();
					break;
			}
		Vector3 kickBack = gunEnd.right.normalized / 10;
		Camera.main.orthographicSize += .01f;
		Camera.main.transform.position += -kickBack;
		yield return new WaitForSeconds (.2f);
		Camera.main.transform.position += kickBack;
		Camera.main.orthographicSize -= .01f;
	}

	IEnumerator MuzzleFlash()
	{
		gunEnd.GetComponent<SpriteRenderer> ().color = Color.white;
		yield return new WaitForSeconds (.05f);
		gunEnd.GetComponent<SpriteRenderer> ().color = Color.clear;
	}

	void ShootStuff()
	{
		//Lose ammo
		currentWeapon.ammo --;
		GameObject newShell = Instantiate(shell, transform.position, Quaternion.identity) as GameObject;
		newShell.GetComponent<Rigidbody2D> ().angularVelocity = shellAngularSpeed;
		newShell.GetComponent<Rigidbody2D> ().velocity = (pushForce / 10) * transform.up.normalized;
		StartCoroutine ("MuzzleFlash");
		//Recoiiiiil
		GetComponent<Rigidbody2D> ().AddForce(-(gunEnd.position - transform.position).normalized * currentWeapon.power);
		//Sound
		AudioSource.PlayClipAtPoint(currentWeapon.sound, transform.position);
	}
	void ShootBow(Vector3 direction)
	{
		GameObject bulletShot = Instantiate(arrow);
		bulletShot.transform.position = gunEnd.position + (gunEnd.right.normalized * 10);
		bulletShot.transform.right = direction.normalized;

	}

	void ShootBullet(Vector3 direction)
	{
		GameObject bulletShot = Instantiate(bullet);
		bulletShot.transform.position = gunEnd.position + gunEnd.right.normalized;
		bulletShot.transform.right = direction.normalized;
		bulletShot.layer = sword.gameObject.layer;
	}

	//Check between player and gun end to make sure you're not shooting from the other side of a wall
	bool WallCheck()
	{
		RaycastHit2D[] wallChecks = Physics2D.RaycastAll (transform.position, gunEnd.position - transform.position, Vector2.Distance (this.transform.position, this.gunEnd.position));
		foreach (RaycastHit2D hit in wallChecks) {
			print (hit.collider.gameObject);
			if(hit.collider.gameObject.tag == "Structure")
				return false;
			}
				return true;
	}
}
