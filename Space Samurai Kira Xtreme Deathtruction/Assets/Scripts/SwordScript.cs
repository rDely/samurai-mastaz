﻿using UnityEngine;
using System.Collections;

public class Weapon
{

	//What weapon ? 
	public enum WeaponType {shotgun, machineGun, gun, bow}
	public WeaponType weaponType;
	public Sprite weaponSprite;

	//How much does it pushback & recoil
	public float power;
	//How much it hurts
	public float damage;
	//Clip size
	public int ammo;
	//Fire rate
	public float fireRate;
	//Sound feedback (in Resources folder)
	public AudioClip sound;

	public Weapon (WeaponType wT)
	{
		//Create a weapon from a given weapon type
		weaponType = wT;
		switch (weaponType) 
		{
		case WeaponType.gun :
			power = 30f;
			damage = 30f;
			ammo = 3;
			fireRate = .3f;
			sound = Resources.Load ("gunSound") as AudioClip;
			weaponSprite = Resources.Load<Sprite>("gunSprite");
			break;
		case WeaponType.machineGun : 
			power = 30f;
			damage = 100f;
			ammo = 12;
			fireRate = 2f;
			sound = Resources.Load ("machineGunSound") as AudioClip;
			weaponSprite = Resources.Load<Sprite>("machineGunSprite") as Sprite;
			break;
		case WeaponType.shotgun :
			power = 150f;
			damage = 100f;
			ammo = 1;
			fireRate = 1.2f;
			sound = Resources.Load ("shotgunSound") as AudioClip;
			weaponSprite = Resources.Load<Sprite>("shotgunSprite") as Sprite;
			break;
		case WeaponType.bow :
			power = 150f;
			damage = 100f;
			ammo = 1;
			fireRate = 1.2f;
			sound = Resources.Load ("bowSound") as AudioClip;
			weaponSprite = Resources.Load<Sprite>("bowSprite") as Sprite;
			break;
		}
	}
}

public class SwordScript : MonoBehaviour {
	
	//Reference for the parent playerscript
	public PlayerScript player;
	//How hard is the pushback for items and players
	float hitForce = 16f;
	//Sound for hitting and parying
	public AudioClip hit;
	public AudioClip cling;
	//References for the slash data
	public SpriteRenderer sRend;
	public PolygonCollider2D polColl;
	public Animator anim;
	//Did the players just parried a slash ?
	public bool parying;

	void Awake()
	{
		//References
		player = transform.parent.parent.GetComponent<PlayerScript> ();
		sRend = GetComponent<SpriteRenderer> ();
		polColl = GetComponent<PolygonCollider2D> ();
	}

	void Start()
	{
		//Deactivate the sprite and collisions detection
		sRend.enabled = false;
		polColl.enabled = false;
		gameObject.layer = 11 + player.GetComponent<PlayerScript> ().playerNumber;
	}



	IEnumerator  Slash()
	{
		anim.SetTrigger ("Slash");
	//Play a sound
		AudioSource.PlayClipAtPoint (hit, transform.position);
	//Activate detection and feedback
		sRend.enabled = true;
		polColl.enabled = true;
		transform.parent.SendMessage ("Slashing");
	//Wait a sec
		yield return new WaitForSeconds (.25f);
	//Deactivate detection
	transform.parent.SendMessage ("Slashing");
	sRend.enabled = false;
	polColl.enabled = false;
	}

	void OnTriggerEnter2D(Collider2D coll)
	{

		//If you hit a sword
	if (coll.tag == "Sword") 
			StartCoroutine ("Pary");
	else if (coll.tag == "Bullet") {
			coll.transform.right = transform.right;
			coll.gameObject.layer = 0;
		} else if (coll.tag == "Shuriken")
			coll.GetComponent<Rigidbody2D> ().velocity = coll.GetComponent<Rigidbody2D> ().velocity.magnitude * player.gunEnd.right;
			//If you hit a player and you haven't just paried
	else if (coll.tag == "Player" && !parying && CheckForWalls(coll))
			StartCoroutine("TouchedSomeone", coll.gameObject);
			//If you hit a crate
	else if (coll.tag == "Crate") 
		coll.GetComponent<CrateScript>().Hit((Vector2)player.gunEnd.right * hitForce, player);
			
	}

	bool CheckForWalls(Collider2D coll)
	{
		RaycastHit2D[] wallCheck = Physics2D.RaycastAll (transform.parent.parent.position, coll.transform.position - transform.parent.parent.position, Vector2.Distance (transform.position, coll.transform.position));
		foreach (RaycastHit2D hit in wallCheck) {
			if (hit.collider.tag == "Structure")
				return false;
		}

		return true;
	}

	IEnumerator TouchedSomeone(GameObject gO)
	{
		yield return new WaitForSeconds (.01f);
		if (!parying)
			Data.data.Die (gO);
	}

	//When you hit another sword
	IEnumerator Pary()
	{
		//Notify the system that you touched a sword so you can't kill someone
		parying = true;
		//Play a nice sound
		AudioSource.PlayClipAtPoint (cling, transform.position);
		//You get pushed back
		transform.parent.parent.GetComponent<Rigidbody2D> ().velocity = - (player.gunEnd.right * hitForce);
		//Wait a bit
		yield return new WaitForSeconds(.5f);
		//Parying phase is over
		parying = false;
	}
		

	}

