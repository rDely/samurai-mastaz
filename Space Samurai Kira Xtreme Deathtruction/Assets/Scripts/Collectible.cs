﻿using UnityEngine;
using System.Collections;

public class Collectible : MonoBehaviour {

	//Power up identifier
	public enum PwrUpType {weapon, consumable};
	public PwrUpType type;

	//If it's a power up
	public enum ConsumableType {grenade, kunai, bow, missile, none};
	public ConsumableType pwrUp;

	//If it's a weapon
	public Weapon.WeaponType wpnTp;


	//When a player pick it up
	void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.tag == "Player") 
		{
			if(type == PwrUpType.weapon)
				coll.GetComponent<PlayerScript>().AssignWeapon(wpnTp);
			else if(type == PwrUpType.consumable)
				coll.GetComponent<PlayerScript>().AssignPowerUp(pwrUp);
			Destroy (this.gameObject);
		}
	}
}
