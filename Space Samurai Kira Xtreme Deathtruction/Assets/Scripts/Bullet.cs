﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {


	public float bulletSpeed;
	public GameObject explosion;

	// Update is called once per frame
	void Update () {
		GetComponent<Rigidbody2D> ().velocity = transform.right * bulletSpeed;
	}

	void OnCollisionEnter2D(Collision2D coll)
	{
		if (coll.collider.tag == "Player")
			Data.data.Die (coll.collider.gameObject);
		else if(coll.collider.tag == "Back")
			Data.data.Die(coll.collider.transform.parent.gameObject);
		else if(coll.collider.tag == "Bullet")
			Instantiate(explosion, transform.position, Quaternion.identity);

		Destroy (this.gameObject);
	}

	void OnBecameInvisible()
	{
		Destroy (gameObject);
	}
}
