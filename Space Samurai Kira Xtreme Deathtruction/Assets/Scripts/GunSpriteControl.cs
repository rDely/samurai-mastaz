﻿using UnityEngine;
using System.Collections;

public class GunSpriteControl : MonoBehaviour {



	
	bool upsideDown;
	int playerNumber;
	void Start()
	{
		playerNumber = transform.parent.parent.GetComponent<PlayerScript> ().playerNumber;
	}

	// Update is called once per frame
	void Update () {
		float horizontalFlip = Input.GetAxis ("Horizontal" + playerNumber.ToString ());
		float verticalFlip = Input.GetAxis ("Vertical"+playerNumber.ToString());
		if (upsideDown && horizontalFlip > 0)
			Flip ();
		else if (!upsideDown && horizontalFlip < 0)
			Flip ();
	}

	void Flip()
	{
		upsideDown = !upsideDown;
		Vector3 theScale = transform.localScale;
		theScale.y *= -1;
		transform.localScale = theScale;
	}
}
