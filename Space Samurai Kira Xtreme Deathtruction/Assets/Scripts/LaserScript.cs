﻿using UnityEngine;
using System.Collections;

public class LaserScript : MonoBehaviour {

	//The lazer
	public GameObject laser;
	//Where I shoot ze lazer
	public Transform shootOrigin;
	public SpriteRenderer feedback;
	public float minRand;
	public float maxRand;
	Animator anim;
	public AnimationClip clip;
	//SHOOT ZEM LAZERZ

	void Awake()
	{
		feedback.enabled = false;
		anim = GetComponentInChildren<Animator> ();
		
	}

	void Start () {
		StartCoroutine ("Pewpewpew", 3f);
	}


	IEnumerator Pewpewpew(float f)
	{
		//Wait for it...
		yield return new WaitForSeconds (10f);
		anim.SetTrigger ("ShootZemLazerz");
		feedback.enabled = true;
		yield return new WaitForSeconds (clip.length);
		feedback.enabled = false;
		//SHOOT ZEM LAZERZ
		GameObject currentLaser = Instantiate (laser, new Vector3(shootOrigin.position.x, shootOrigin.position.y, 2), transform.rotation) as GameObject;
		//FRRRRRTTRTRETERTRTHZZZZZZZZ
		StartCoroutine ("Shake", f);
		yield return new WaitForSeconds (f);
		//OK STOP
		Destroy (currentLaser);
		//And start over
		StartCoroutine ("Pewpewpew", 3f);
	}

	IEnumerator Shake(float f)
	{
		for(int i = 0; i <25 ; i++)
		{
			float randomX = Random.Range (minRand, maxRand);
			float randomY = Random.Range (minRand, maxRand);
			Camera.main.transform.position = new Vector3 (randomX, randomY, Camera.main.transform.position.z);
			yield return new WaitForSeconds (f/50);
			Camera.main.transform.position = new Vector3 (0, 0, Camera.main.transform.position.z);
			yield return new WaitForSeconds (f/50);
		}
	}

}
