﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Data : MonoBehaviour {

	//Singleton for game control
	static public Data data;
	public bool gameOn;
	public GameObject powerUp;
	//Where the players should start (Array)
	public Transform[] spawnArray;
	//For winner tracking (list)
	List<string> activePlayers = new List<string>{"Inactive", "Inactive", "Inactive", "Inactive"};
	//The winning screen
	public Canvas winUI;
	public Canvas setUpUI;
	//The winning text
	public Text winCaption;
	public Text countDown;
	public GameObject player;
	bool notTheFirstTime;
	int howManyPlayers;
	public Image[] preMatchCutscene;

	void Awake()
	{
		data = this;
	}

	public void LaunchGame(bool b, int i)
	{
		if (b) {
		setUpUI.gameObject.SetActive (true);
		} 
		else 
			SpawnPlayers (i);
	}
	public void SpawnPlayers(int i)
	{
		Score.score.howManyPlayers = i;
		notTheFirstTime = true;
		StartCoroutine ("WaitForIt", i);
	}

	IEnumerator WaitForIt(int i)
	{
		for (int j = 0; j < i; j++) 
		{
			preMatchCutscene[j].GetComponentInChildren<Text>().text = Score.score.playerTags[j];
			preMatchCutscene[j].GetComponentInChildren<Image>().sprite = Score.score.playersFaces[j];
		}
		for (int j = 0; j < i; j++) 
		{
			GameObject p = Instantiate<GameObject>(player);
			p.transform.position = spawnArray[j].position;
			p.GetComponent<PlayerScript>().nametag.text = Score.score.playerTags[j];
			activePlayers.RemoveAt(j); 
			activePlayers.Insert(j, "Active");
			p.GetComponent<PlayerScript>().playerNumber = j + 1;
			p.GetComponentInChildren<GunControl>().playerNumber = j + 1;
			p.GetComponent<PlayerScript>().back.layer = j + 8;
			p.gameObject.layer = 8+j;
			p.GetComponent<PlayerScript>().nametag.text = Score.score.playerTags[j];
			StartCoroutine("InitiateSpeed", p);
		}
		countDown.text = "3";
		countDown.transform.parent.gameObject.SetActive (true);
		yield return new WaitForSeconds (.5f);
		countDown.text = "2";
		yield return new WaitForSeconds (.5f);
		countDown.text = "1";
		yield return new WaitForSeconds (.5f);
		countDown.text = "FAHITO BIGINESU!";
		gameOn = true;
		yield return new WaitForSeconds (1f);
		countDown.transform.parent.gameObject.SetActive (false);	
	}

	IEnumerator InitiateSpeed(GameObject gO)
	{
		yield return new WaitForSeconds (1.5f);
		gO.GetComponent<Rigidbody2D> ().velocity = - (gO.transform.position / 1.1f);
	}

	//Track all the players that are in the game
	//Maybe this requires rewriting

	//To kill a mocking samurai
	public void Die (GameObject G) {
		int playerNumber = G.GetComponent<PlayerScript> ().playerNumber;
		Destroy (G);

		//Remove him from the player list
		activePlayers [playerNumber - 1] = "Dead";
		//Check if there's only one player left
		CheckForWinner ();

	}

	//
	void CheckForWinner()
	{
		int playersLeft = 0;
		foreach (string s in activePlayers) 
			{
				if (s == "Active") 
					playersLeft ++;
			}
		if (playersLeft == 0)
			StartCoroutine ("WeHaveAWinner", -1);
		if (playersLeft <= 1)
			StartCoroutine("WeHaveAWinner", activePlayers.IndexOf ("Active"));
	}


	IEnumerator WeHaveAWinner(int i)
	{

		yield return new WaitForSeconds (1.5f);
		winUI.gameObject.SetActive(true);
		if (i == -1)
			winCaption.text = "TYE";
		else 
			{
				winCaption.text = "Player " + (i + 1) + " is the greatest warrior";
				Score.score.scoreArray[i] ++;
			}
			yield return new WaitForSeconds(1.5f);
		int j = 0;
		print (Score.score.scoreArray[0] + " " + Score.score.scoreArray[1] + " " + Score.score.scoreArray[2] + " " + Score.score.scoreArray[3]);
		foreach(int id in Score.score.scoreArray)
			{

				if (id >= 10)
			{
				Win(i);
				j ++;
			}
		}
		if(j == 0)
			Application.LoadLevel ("LevelDesignMorgan");
	}

		void Win(int i)
	{
		winCaption.text = "Player " + (i + 1) + " is the greatest warrior for REAL";
	}
}
