﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Score : MonoBehaviour {
	public static Score score;
	public int howManyPlayers;
	public AudioClip splashSound;
	bool firstTime = true;
	public int[] scoreArray = {0,0,0,0};
	public string[] playerTags = {"", "", "", ""};
	int currentFocus;
	public Image splashScreen;
	public Sprite[] playersFaces;

	// Use this for initialization
	void Awake () {
	if (score == null)
			score = this;
		else if (score != this)
			Destroy (gameObject);
		DontDestroyOnLoad (this.gameObject);
	
	}

	void Start()
	{
		StartCoroutine ("DUMPLINGSAAAAAANDWICH");
	}

	public void ChangeNameTag(string s)
	{
		playerTags[currentFocus] = s;
	}

	public void ChangeFocus(int i)
	{
		currentFocus = i;
	}

	IEnumerator DUMPLINGSAAAAAANDWICH()
	{
		yield return new WaitForSeconds (splashSound.length);
		splashScreen.gameObject.SetActive(false);
	}

	public void Launchgame()
	{
		Application.LoadLevel ("LevelDesignMorgan");
	}

	void OnLevelWasLoaded(int level)
	{
		if (level == 1) {
			if (firstTime)
				Data.data.LaunchGame (true, 0);
			else 
				Data.data.LaunchGame (false, howManyPlayers);
			firstTime = false;
			}
	}

}