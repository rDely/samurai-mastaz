﻿using UnityEngine;
using System.Collections;

public class CratesSpawner : MonoBehaviour {
	float powerUpInitialSpeed = 10f;
	public GameObject woodenCrate;
	public GameObject steelCrate;
	public GameObject spikedCrate;
	public GameObject bento;
	public GameObject shotgun;
	public GameObject machineGun;
	public GameObject missileLauncher;
	public GameObject bow;
	public GameObject kunai;
	public GameObject dynamite;
	public GameObject grenade;
	public bool powerUpSpawner;
	GameObject spwned;
	// Use this for initialization
	void Start () {
		ChoosePowerUp();
	}

	void RandomCrate()
	{
		int spawnRandomizer = Random.Range (0, 100);
		if (spawnRandomizer < 20)
			Spawn (woodenCrate);
		else if (spawnRandomizer < 40)
			Spawn (steelCrate);
		else if (spawnRandomizer < 60)
			Spawn (spikedCrate);
		else if (spawnRandomizer < 80)
			ChoosePowerUp ();
		else
			Spawn (dynamite);
	}

	void ChoosePowerUp ()
	{
		
		int pwrUpRandomizer = Random.Range (0, 100);
		if (pwrUpRandomizer < 15)
			Spawn (shotgun);
		else if (pwrUpRandomizer < 30)
			Spawn (machineGun);
		else if (pwrUpRandomizer < 45)
			Spawn (missileLauncher);
		else if (pwrUpRandomizer < 60)
			Spawn (bow);
		else if (pwrUpRandomizer < 75)
			Spawn (grenade);
		else 
			Spawn (kunai);

	}

	void Spawn(GameObject obj)
	{
		spwned = Instantiate (obj) as GameObject;
		spwned.transform.position = transform.position;
		Data.data.powerUp = spwned;
		//spwned.GetComponent<Rigidbody2D> ().velocity = transform.right * powerUpInitialSpeed;
		StartCoroutine ("RandomWaitBetweenSpawns");

	}



	IEnumerator RandomWaitBetweenSpawns()
	{
		int f = Random.Range (5, 10);
		while (Data.data.powerUp != null) {
			yield return new WaitForSeconds (.1f);
		}	
		yield return new WaitForSeconds (f);
		if (powerUpSpawner)
			ChoosePowerUp ();
		else 
			RandomCrate ();

	}
}
