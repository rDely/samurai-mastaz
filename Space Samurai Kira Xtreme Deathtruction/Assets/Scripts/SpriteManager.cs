﻿using UnityEngine;
using System.Collections;

public class SpriteManager : MonoBehaviour {


	bool facingRight = true;
	// Use this for initialization
	void Start () {
	
	}


	// Update is called once per frame
	void Update () {
		if (facingRight && Input.GetAxis("Horizontal" + transform.parent.GetComponent<PlayerScript>().playerNumber) < 0)
			Flip ();
		else if (!facingRight && Input.GetAxis("Horizontal" + transform.parent.GetComponent<PlayerScript>().playerNumber) > 0)
			Flip ();
	}

	void Flip()
	{
		facingRight = !facingRight;
		transform.localScale = new Vector3 (- transform.localScale.x, 1, 1);	
	}

}
